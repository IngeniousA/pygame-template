import pygame
import sys
from math import sin, cos, pi

size = width, height = 800, 600
black = 0, 0, 0
screen = pygame.display.set_mode(size)

class Ball:
    def __init__(self, inangle):
        self.image = pygame.image.load('basketball.png')
        self.rect = self.image.get_rect()
        self.rect.x = 100
        self.rect.y = 100
        self.r = 50
        self.angle = inangle
        self.prevX = self.rect.x
        self.prevY = self.rect.y
        self.step = 0

    def draw(self):
        screen.blit(self.image, self.rect)

    def set_angle(self, angle):
        self.angle = angle

    def bounce(self):
        self.angle += pi / 2
   
    def move(self):
        if self.step % 45 == 0:
            self.prevX = self.rect.x
            self.prevY = self.rect.y
        self.rect.x += self.r * cos(self.angle)
        self.rect.y += self.r * sin(self.angle)
        self.step += 1

    def is_free(self):
        return self.rect.x >= self.r and self.rect.x <= width - 2 * self.r \
               and self.rect.y >= self.r and self.rect.y <= height - 2 * self.r

    def stub(self):
        return abs(self.rect.x - self.prevX) < 5 or abs(self.rect.y - self.prevY) < 5

    def kick(self):
        if self.step % 45 == 0:
            self.prevX = self.rect.x
            self.prevY = self.rect.y
        self.rect.x += 15 * cos(self.angle)
        self.rect.y += 15 * sin(self.angle)
        self.step += 1

def main():
    pygame.init()
    gameover = False
    ball = Ball(3 * pi / 4)
    while not gameover:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                gameover = True
        screen.fill(black)
        ball.draw()
        if ball.is_free():
            ball.move()
        else:
            # print(ball.rect.x, ball.rect.y)
            ball.bounce()
            ball.move()
        # if ball.stub():
        #    ball.kick()
        pygame.display.flip()
        pygame.time.wait(25)
    sys.exit()

main()
